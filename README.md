# com.hcnx.otp

[![CI Status](http://img.shields.io/travis/Guillaume MARTINEZ/com.hcnx.otp.svg?style=flat)](https://travis-ci.org/Guillaume MARTINEZ/com.hcnx.otp)
[![Version](https://img.shields.io/cocoapods/v/com.hcnx.otp.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.otp)
[![License](https://img.shields.io/cocoapods/l/com.hcnx.otp.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.otp)
[![Platform](https://img.shields.io/cocoapods/p/com.hcnx.otp.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.otp)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

com.hcnx.otp is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "com.hcnx.otp"
```

## Author

Guillaume MARTINEZ, g.martinez@highconnexion.com

## License

com.hcnx.otp is available under the MIT license. See the LICENSE file for more info.
