//
//  OTPFramework.h
//  OTPFramework
//
//  Created by Damien PRACA on 03/10/16.
//  Copyright © 2016 HighConnexion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCNXBase/HCNXBase.h>

//! Project version number for OTPFramework.
FOUNDATION_EXPORT double OTPFrameworkVersionNumber;

//! Project version string for OTPFramework.
FOUNDATION_EXPORT const unsigned char OTPFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OTPFramework/PublicHeader.h>
#import <Foundation/Foundation.h>

@interface OTP : HCNXBase
@property (nonatomic , strong) NSString * mVoucher;

/*!
 @brief Use this method to get the instance of OTP
 */
+ (OTP *)sharedInstance;


/*!
 @brief Use this method to generate a voucher and a short code that you will receive by sms to the provided number.
 This short code(also named "challenge") and voucher then has to be used in the authenticate method
 */
- (void) generate:(NSString*)phone completion:(void (^)(NSError* error)) onCompletion;

/*!
 @brief Use this method to authenticate and receive a unique session token.
 To authenticate you need the challenge code received by SMS and the voucher provided by the generate method.
 For you own database you just have to associate the phone number (aka user id) and the provided token and expire the token on your side after few minutes and send back to client a 401.
 Upon receiving this 401 your client code has to call the renew method with the refreshToken and update your database with the new token.
 */
- (void) authenticate:(NSString*)challenge completion:(void (^)(NSString* userId, NSString* token, NSError* error)) onCompletion;

/*!
 @brief Use this method to renew a session token.
 */
- (void) renewOnCompletion:(void (^)(NSString* token, NSError* error)) onCompletion;

/*!
 @brief Use this method to get the associated user of a specific session token.
 */
- (void) getUserWithToken:(NSString*)token completion:(void (^)(NSDictionary* userGraph, NSError* error)) onCompletion;



@end
