#
# Be sure to run `pod lib lint com.hcnx.otp.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'com.hcnx.otp.rec'
  s.version          = '4.0.2'
  s.summary          = 'HCNX OTP.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Intregration of OTP via CocoaPod.
                       DESC

  s.homepage         = 'https://bitbucket.org/devmobile/com.hcnx.otp.rec.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guillaume MARTINEZ' => 'g.martinez@highconnexion.com' }
  s.source           = { :git => 'https://bitbucket.org/devmobile/com.hcnx.otp.rec.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.ios.vendored_frameworks = '*.framework'
  s.dependency 'com.hcnx.hcnx_base.rec'
end
